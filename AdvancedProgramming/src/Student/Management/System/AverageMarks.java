package Student.Management.System;

public class AverageMarks extends TotalMarks {
	private double average;

	public AverageMarks(String name, int grade, char division, int maths_marks, int science_marks, int tamil_marks,
			int total, double average) {
		super(name, grade, division, maths_marks, science_marks, tamil_marks, total);
		
		average=total/3;
		this.average = average;
	}
	

}
