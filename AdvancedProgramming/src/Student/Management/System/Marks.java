package Student.Management.System;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Marks extends Function {
	static Scanner scan= new Scanner(System.in);
	static Scanner in= new Scanner(System.in);
	static Scanner input= new Scanner(System.in);
	private static String url ="jdbc:mysql://localhost:3306/";
	private static String username ="root";
	private static String password ="KOBI8324";
	private static String database="StudentManagementSystem";
	Connection connection = null;
    Statement myStmt = null;
    ResultSet resultset = null;
    
	public Marks() {
		super(FunctionType.MARKS);
		arrangeProgram();
		marksDetails();
	}
	
	
	@Override
	protected void arrangeProgram() {
		System.out.println("Enter the student marks one by one");
		
	}
	
	protected void marksDetails() {
		String option="y";
		 while(option.equalsIgnoreCase("y")) {
		try {
			connection = DriverManager.getConnection(url+database,username,password);
		
		 
        System.out.println("Select the division");
	        System.out.println("1.11A");
	        System.out.println("2.11B");
	        System.out.println("3.11C");
	      
	        
	         int y = input.nextInt();
	         if(y==1) {
	        	String query1 = "SELECT name FROM studentdetails where grade='11' and stu_class='A'";
				PreparedStatement pst1 = connection.prepareStatement(query1);
            

				try {
					resultset = pst1.executeQuery();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					while(resultset.next()) {
						System.out.println("student  Name = " + resultset.getString(1) );
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
	        	 
	        	 System.out.println();
	        	System.out.println("Enter the student name:");
	        	 String stuName=scan.next();
	        	 System.out.println("Enter the maths marks:");
	        	 int maths=scan.nextInt();
	        	System.out.println("Enter the science marks:");
	        	 int science=scan.nextInt();
	        	System.out.println("Enter the  Tamil marks:");
	        	 int tamil=scan.nextInt();
	        	 int total_Marks=maths+science+tamil;
	        	 double average=total_Marks/3;
	        	 
	        	 myStmt = connection.createStatement(); 
	            String sql11 = "insert into 11a_marksheet " + " (name,maths,science,tamil,totalMarks,average)"
	                    + " values ('"+ stuName+"','"+ maths+"','"+science+"','"+tamil+"','"+total_Marks+"','"+average+"')";
	 
	            myStmt.executeUpdate(sql11);
	 
	            System.out.println("Insert complete in the marksheet.");
	            
	          myStmt.close();
	         
	        	 
	         }
	         else if(y==2) {
	        	String qu = "SELECT name FROM studentdetails where grade='11' and stu_class='B'";
				PreparedStatement prSt = connection.prepareStatement(qu);
           

				resultset =prSt.executeQuery();
				
				while(resultset.next()) {
					System.out.println("student  Name = " + resultset.getString(1) );
				}
				
				
				
	        	 
	        	 System.out.println();
	        	System.out.println("Enter the student name:");
        	 String stuName1=scan.next();
	        	 System.out.println("Enter the maths marks:");
	        	 int maths1=scan.nextInt();
	        	System.out.println("Enter the science marks:");
        	 int science1=scan.nextInt();
        	System.out.println("Enter the  Tamil marks:");
        	 int tamil1=scan.nextInt();
        	 int total_Marks1=maths1+science1+tamil1;
        	 double average1=total_Marks1/3;
        	 
        	 myStmt = connection.createStatement(); 
	            String sql_1 = "insert into 11b_marksheet " + " (name,maths,science,tamil,totalMarks,average)"
	                    + " values ('"+ stuName1+"','"+ maths1+"','"+science1+"','"+tamil1+"','"+total_Marks1+"','"+average1+"')";
	 
	            myStmt.executeUpdate(sql_1);
	 
	            System.out.println("Insert complete in the marksheet.");
          myStmt.close();
          
	         }
	         else {
      	String qu1 = "SELECT name FROM studentdetails where grade='11' and stu_class='C'";
			PreparedStatement prSt1 = connection.prepareStatement(qu1);
        

			resultset =prSt1.executeQuery();
			
			while(resultset.next()) {
				System.out.println("student  Name = " + resultset.getString(1) );
			}
			
			
			
      	 
      	 System.out.println();
      	System.out.println("Enter the student name:");
     	 String stuName11=scan.next();
      	 System.out.println("Enter the maths marks:");
      	 int maths11=scan.nextInt();
      	System.out.println("Enter the science marks:");
     	 int science11=scan.nextInt();
     	System.out.println("Enter the  Tamil marks:");
     	 int tamil11=scan.nextInt();
     	 int total_Marks11=maths11+science11+tamil11;
     	 double average11=total_Marks11/3;
     	 
     	 myStmt = connection.createStatement(); 
          String sql_11 = "insert into 11c_marksheet " + " (name,maths,science,tamil,totalMarks,average)"
                  + " values ('"+ stuName11+"','"+ maths11+"','"+science11+"','"+tamil11+"','"+total_Marks11+"','"+average11+"')";

          myStmt.executeUpdate(sql_11);

          System.out.println("Insert complete in the marksheet.");
       myStmt.close();
	         
          }
          
	
		 }	 catch (SQLException e)  {
	            System.out.println("SQLException: "+e.getMessage());
	        }
			System.out.println("Would you like to try again ? (enter y for yes)");
	        option = in.nextLine();
	        System.out.println("Thank you");
	   }
/*	private int maths_marks;
	private int science_marks;
	private int tamil_marks;
	
	public Marks(String name, int grade, char division, int maths_marks, int science_marks, int tamil_marks) {
		super(name, grade, division);
		this.maths_marks = maths_marks;
		this.science_marks = science_marks;
		this.tamil_marks = tamil_marks;
	}
	public int getMaths_marks() {
		return maths_marks;
	}
	public void setMaths_marks(int maths_marks) {
		this.maths_marks = maths_marks;
	}
	public int getScience_marks() {
		return science_marks;
	}
	public void setScience_marks(int science_marks) {
		this.science_marks = science_marks;
	}
	public int getTamil_marks() {
		return tamil_marks;
	}
	public void setTamil_marks(int tamil_marks) {
		this.tamil_marks = tamil_marks;
	}*/
	
	
	

	}
}


