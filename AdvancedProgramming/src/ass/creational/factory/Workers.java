package ass.creational.factory;

public abstract class Workers {
	
protected WorkType position;
	
	protected abstract void arrangeProgram();
	public Workers(WorkType position) {
		this.position=position;
		allocateWork();
		
	}
	
	private void allocateWork() {
		System.out.println("Allocate the work for "+position);
	}
	public WorkType getPosition() {
		return position;
	}
	public void setPosition(WorkType position) {
		this.position = position;
	}
	
	

	

}
