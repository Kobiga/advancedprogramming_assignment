package ass.creational.factory;

public class Manager extends Workers {

	public Manager() {
		super(WorkType.MANAGER);
		arrangeProgram();
	}

	@Override
	protected void arrangeProgram() {
		System.out.println("Prepare the admin,lectures program");
		
	}
}