package ass.oop.abs;

public abstract class Car {
	abstract void run();

}

class BMWcar extends Car{

//String color="Blue";
@Override
void run() {
	System.out.println("BMW car runs smoothly");
	
}

}
class Hyundaicar extends Car{

//String color="Black";
@Override
void run() {
	System.out.println("hyundai car runs smoothly and fast");
	
}

}
