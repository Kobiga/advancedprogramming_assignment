package ass.vehicle.rented;

public class Bicycle extends RentedVehicle{
	private int nbDays; // number of days rented

	public Bicycle(double baseFees) {
		super(baseFees);
		
	}

	public Bicycle(double baseFees, int nbDays) {
		super(baseFees);
		this.nbDays = nbDays;
	}

	public int getNbDays() {
		return nbDays;
	}
	public void setNbDays(int nbDays) {
		this.nbDays = nbDays;
	}

	public double getCost() {
		return nbDays*getBaseFees();
	}
	

}
