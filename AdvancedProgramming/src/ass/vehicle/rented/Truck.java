package ass.vehicle.rented;

public class Truck extends FuelVehicle {
	private int capacity;

	public Truck(double baseFees, double nbKms) {
		super(baseFees, nbKms);
		
	}
	public double getMileageFees() {
		return super.getMileageFees();
}
	public double getCost() {
		return (capacity*getBaseFees())+getMileageFees();
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getCapacity() {
		return capacity;
	}
	

}
